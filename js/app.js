/**
 * Created by petit on 04/05/2017.
 */

var angularApp = angular.module("app",['ngMaterial','ngResource']);//Définit l'application

angularApp.controller("TodoCtrl",function ($scope, $filter, Todo) {//Permet de créer un controller
    $scope.currentEdit = {};

    $scope.maListe = [];
    $scope.currentPage = 1;

    $scope.addEventOnTodoList = function () {
        Todo.save($scope.currentEdit).$promise.then(function (data) {
            $scope.currentEdit = {};
            $scope.maListe.push(data);
            changeListe();
        });
    };

    $scope.supprEventOnTodoList = function (obj) {
        Todo.remove(obj).$promise.then(function (data) {

            if($scope.maListeResults !== undefined){
                var index = $scope.maListeResults.findIndex(x => x.id==obj.id);
                $scope.maListeResults.splice(index,1);
            }
            var index2 = $scope.maListe.findIndex(x => x.id==obj.id);
            $scope.maListe.splice(index2,1);

            changeListe();
        });
    };

    $scope.editEventOnTodoList = function (obj) {
        $scope.currentEdit = obj;
    };

    $scope.sendEditEventOnTodoList = function () {
        if($scope.isInEditMode()){
            Todo.update($scope.currentEdit).$promise.then(function (data) {
                $scope.currentEdit = {};
                changeListe();
            });
        }
    };

    $scope.changeStateEventOnTodoList = function (obj) {
        Todo.update(obj).$promise.then(function (data) {
            changeListe();
        });
    };

    $scope.isInEditMode = function() {
        return $scope.currentEdit.id !== undefined && $scope.currentEdit.id !== null;
    };
    $scope.currentPageExist = function() {
        return $scope.currentPage !== undefined && $scope.currentPage !== null;
    };

    //Pagination
    var changeListe = function () {
        liste = $scope.maListe;
        if($scope.maListeResults !== undefined){
            if($scope.maListe.length === $scope.maListe.maListeResults){
                liste = $scope.maListe;
            }else{
                liste = $scope.maListeResults;
            }
        }
        $scope.maListeInCurrentPage = liste.slice($scope.currentPage*10-10,$scope.currentPage*10);
        $scope.maListePageLength = Math.ceil(liste.length/10);

        if($scope.maListeInCurrentPage.length < 1 && $scope.maListePageLength > 0){
            $scope.currentPage = $scope.maListePageLength;
            changeListe();
        }
    };

    $scope.range = function (min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) input.push(i);
        return input;
    };

    $scope.nextPage = function () {
        if($scope.currentPage !== $scope.maListePageLength){
            $scope.currentPage++;
            changeListe();
        }
    };

    $scope.previousPage = function () {
        if($scope.currentPage > 1){
            $scope.currentPage--;
            changeListe();
        }
    };

    $scope.goPage = function (page) {
        if(page <= $scope.maListePageLength){
            $scope.currentPage = page;
            changeListe();
        }
    };

    $scope.search = function () {
        $scope.currentPage = 1;
        var results = $filter('filter')($scope.maListe, $scope.searchValue);
        $scope.maListeResults = results;
        changeListe();
    };

    // Avec la base de donnée
    Todo.query(
        function (data) {
            $scope.maListe = data;
            changeListe();
        },function (error) {
            alert("Error "+error.status)
        }
    );
});

angularApp.factory('Todo', ['$resource',
    function ($resource) {
        var transformTodoFromResponse = function (data) {
            data = JSON.parse(data);
            data = {'id': data.id, 'state': data.completed, 'name': data.title, 'place': data.userId};
            return data;
        };
        var transformTodosFromResponse = function (data) {
            data = JSON.parse(data);
            angular.forEach(data, function (value, key) {
                this[key] = {'id': value.id, 'state': value.completed, 'name': value.title, 'place': value.userId};
            }, data);
            return data;
        };
        var transformTodoForRequest = function (data) {
            data = { "userId": data.place, "id": data.id, "title": data.name, "completed": data.state === undefined ? false : data.state};
            return JSON.stringify(data);
        };
        return $resource(
            'http://localhost:3000/todos/:id', {id: '@id'},
            {
                'query': {
                    method: 'GET',
                    isArray: true,
                    transformResponse: transformTodosFromResponse
                },
                'get':      { method:'GET', transformResponse: transformTodoFromResponse },
                'save':     { method:'POST', transformResponse: transformTodoFromResponse, transformRequest: transformTodoForRequest },
                'remove':   { method:'DELETE' },
                'delete':   { methode:'DELETE' },
                'update':   { method:'PUT', transformResponse: transformTodoFromResponse, transformRequest: transformTodoForRequest  }
            }
        )
    }]
);

angularApp.directive('maTodoListRow',function () {
    return {
        restrict:'EA',
        replace:true,
        // template:'<h3>wtf</h3>'
        templateUrl:'./Templates/todoListRow.html',
        scope:{btnSuppr:"=maTodoListSuppr", btnEdit:"=maTodoListEdit", btnState:"=maTodoListChangeState", key:'=maTodoListKey', value:"=maTodoListValue"}
            // link:function (scope, element, attrs) {
            //     scope.btnSuppr = function (index) {
            //
            //     }
            // }
    }
});